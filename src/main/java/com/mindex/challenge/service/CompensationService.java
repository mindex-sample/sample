package com.mindex.challenge.service;

import com.mindex.challenge.data.Compensation;
import com.mindex.challenge.util.error.RequestError;

public interface CompensationService {
	Compensation getComp(String id) throws RequestError;
	Compensation createComp(Compensation comp) throws RequestError;
	Compensation updateComp(Compensation comp) throws RequestError; 
}
