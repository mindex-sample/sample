package com.mindex.challenge.service;

import com.mindex.challenge.controller.response.ReportingStructure;
import com.mindex.challenge.util.error.RequestError;

public interface ReportingService {
	ReportingStructure getStructure(String id) throws RequestError;
}
