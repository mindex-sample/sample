package com.mindex.challenge.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mindex.challenge.dao.CompensationRepository;
import com.mindex.challenge.dao.EmployeeRepository;
import com.mindex.challenge.data.Compensation;
import com.mindex.challenge.service.CompensationService;
import com.mindex.challenge.util.error.RequestError;

@Service
public class CompensationServiceImpl implements CompensationService {
	
	@Autowired
	private CompensationRepository compRepo;
	
	@Autowired
	private EmployeeRepository employeeRepo;
	
	
	//avoid creating a comp for a employee that does not exist
	@Override
	public Compensation getComp(String id) throws RequestError {
		Compensation comp = compRepo.findByEmployeeId(id);
		if(comp == null)
			throw new RequestError(404, "No comp found"); 
		return comp;
	}

	//only create a report if a report has not been created before, since id is based on employee rather than an individual comp
	@Override
	public Compensation createComp(Compensation comp) throws RequestError {
		if(employeeRepo.findByEmployeeId(comp.getEmployeeId()) == null)
			throw new RequestError(500, "No employee for that id"); 
		if(compRepo.findByEmployeeId(comp.getEmployeeId()) != null)
			throw new RequestError(500, "Comp already exists, please update instead"); 
		compRepo.save(comp);
		return comp;
	}

	//only update if the comp already exists
	@Override
	public Compensation updateComp(Compensation comp) throws RequestError {
		if(compRepo.findByEmployeeId(comp.getEmployeeId()) == null)
			throw new RequestError(500, "No comp exists");
		compRepo.save(comp);
		return comp;
	}

}
