package com.mindex.challenge.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mindex.challenge.controller.response.ReportingStructure;
import com.mindex.challenge.dao.EmployeeRepository;
import com.mindex.challenge.data.Employee;
import com.mindex.challenge.service.ReportingService;
import com.mindex.challenge.util.error.RequestError;

@Service
public class ReportingServiceImpl implements ReportingService {

	@Autowired
    private EmployeeRepository employeeRepository;
	
	@Override
	public ReportingStructure getStructure(String id) throws RequestError{
		Employee employee = employeeRepository.findByEmployeeId(id); 
		if(employee == null)
			throw new RequestError(404, "No employee exists"); //throw a error
		int totalReports = this.getTotalReportsForEmployee(employee);
		
		ReportingStructure response = new ReportingStructure();
		response.setEmployee(employee);
		response.setNumberOfReports(totalReports);
		
		return response;
	}
	
	//assume no dupps 
	private int getTotalReportsForEmployee(Employee employee) {
		if(employee.getDirectReports() == null)
			return 0;
		int total = 0;
		for(String id : employee.getDirectReports()) {
			Employee ee = employeeRepository.findByEmployeeId(id);
			if(ee != null)
				total+=1+this.getTotalReportsForEmployee(ee);
		}
		return total;
	}
	
}
