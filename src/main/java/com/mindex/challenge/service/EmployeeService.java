package com.mindex.challenge.service;

import com.mindex.challenge.data.Employee;
import com.mindex.challenge.util.error.RequestError;

public interface EmployeeService {
    Employee create(Employee employee);
    Employee read(String id) throws RequestError;
    Employee update(Employee employee) throws RequestError;
}
