package com.mindex.challenge.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.mindex.challenge.service.ReportingService;
import com.mindex.challenge.util.GeneralUtil;
import com.mindex.challenge.util.error.RequestError;

@RestController
public class ReportingController {
	
	@Autowired
	private ReportingService reportingService;

	
	@GetMapping("/structure/{id}")
	public ResponseEntity<Object> getStructure(@PathVariable String id) {
		try {
			return ResponseEntity.ok(reportingService.getStructure(id));
		} catch (RequestError e) {
			return GeneralUtil.getErrorResponse(e);
		}
	}
}
