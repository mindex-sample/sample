package com.mindex.challenge.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.mindex.challenge.data.Compensation;
import com.mindex.challenge.service.CompensationService;
import com.mindex.challenge.util.GeneralUtil;
import com.mindex.challenge.util.error.RequestError;

@RestController
public class CompController {
	
	@Autowired
	private CompensationService compService;

	@GetMapping("/comp/{id}")
	public ResponseEntity<Object> getComp(@PathVariable String id) {
		try {
			return ResponseEntity.ok(compService.getComp(id));
		} catch (RequestError e) {
			return GeneralUtil.getErrorResponse(e); 
		}
	}
	
	@PostMapping("/comp") //assume id is already in the comp
	public ResponseEntity<Object> createComp(@RequestBody Compensation comp) {
		try {
			return ResponseEntity.ok(compService.createComp(comp));
		} catch (RequestError e) {
			return GeneralUtil.getErrorResponse(e);
		}
	}
	
	@PutMapping("/comp") //assume id is already in the comp
	public ResponseEntity<Object> updateComp(@RequestBody Compensation comp) {
		try {
			return ResponseEntity.ok(compService.updateComp(comp));
		} catch (RequestError e) {
			return GeneralUtil.getErrorResponse(e);
		}
	}
	
}
