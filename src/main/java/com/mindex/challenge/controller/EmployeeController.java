package com.mindex.challenge.controller;

import com.mindex.challenge.data.Employee;
import com.mindex.challenge.service.EmployeeService;
import com.mindex.challenge.util.GeneralUtil;
import com.mindex.challenge.util.error.RequestError;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class EmployeeController {
    private static final Logger LOG = LoggerFactory.getLogger(EmployeeController.class);

    @Autowired
    private EmployeeService employeeService;

    @PostMapping("/employee")
    public ResponseEntity<Object> create(@RequestBody Employee employee) {
        LOG.debug("Received employee create request for [{}]", employee);

        return ResponseEntity.ok(employeeService.create(employee));
    }

    @GetMapping("/employee/{id}")
    public ResponseEntity<Object> read(@PathVariable String id) {
        LOG.debug("Received employee create request for id [{}]", id);

        try {
			return ResponseEntity.ok(employeeService.read(id));
		} catch (RequestError e) {
			return GeneralUtil.getErrorResponse(e);
		}
    }

    @PutMapping("/employee/{id}")
    public ResponseEntity<Object> update(@PathVariable String id, @RequestBody Employee employee) {
        LOG.debug("Received employee create request for id [{}] and employee [{}]", id, employee);

        employee.setEmployeeId(id);
        try {
			return ResponseEntity.ok(employeeService.update(employee));
		} catch (RequestError e) {
			return GeneralUtil.getErrorResponse(e);
		}
    }
}
