package com.mindex.challenge.util.error;

public class RequestError extends Exception {
	
	private static final long serialVersionUID = 1L;

	private int type;
	private String requestError;
	
	public RequestError(int type, String requestError) {
		this.type = type;
		if(this.type == -1)
			type = 500;
		this.requestError = requestError;
	}
	
	public int getType() {
		return type;
	}
	
	public String getRequestError() {
		return requestError;
	}

}
