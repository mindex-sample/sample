package com.mindex.challenge.util;

import org.springframework.http.ResponseEntity;

import com.mindex.challenge.util.error.RequestError;
import com.mindex.challenge.util.response.BasicResponse;

public class GeneralUtil {
	public static ResponseEntity<Object> getErrorResponse(RequestError error) {
		return ResponseEntity.status(error.getType()).body(new BasicResponse(error.getRequestError()));	
	}
}
