package com.mindex.challenge.service.impl;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import com.mindex.challenge.data.Compensation;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class CompensationServiceImplTest {
	
	private String createUrl;
	private String updateUrl;
	private String getUrl;
	
	@LocalServerPort
	private int port;
	
	@Autowired
	private TestRestTemplate restTemplate;
	
	@Before
	public void setup() {
		createUrl = "http://localhost:" + port + "/comp";
		updateUrl = "http://localhost:" + port + "/comp";
		getUrl = "http://localhost:" + port + "/comp/{id}";
	}
	
	@Test
	public void testAll() {
		Compensation testComp = new Compensation();
		testComp.setEffectiveDate(new Date());
		testComp.setSalary(new BigDecimal(50000));
		testComp.setEmployeeId("16a596ae-edd3-4847-99fe-c4518e82c86f");
		
		//create the comp and check if matches
		ResponseEntity<Compensation> response = restTemplate.postForEntity(createUrl, testComp, Compensation.class);
		assertEquals(200, response.getStatusCodeValue()); //check to make sure it worked
		assertEqualComp(testComp, response.getBody());
		
		//get test
		response = restTemplate.getForEntity(getUrl, Compensation.class, testComp.getEmployeeId());
		assertEquals(response.getStatusCodeValue(), 200); //check to make sure it worked
		assertEqualComp(testComp, response.getBody());
		
		HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
		
		//update test
		testComp.setSalary(new BigDecimal(60000));
		response = restTemplate.exchange(updateUrl, HttpMethod.PUT, new HttpEntity<Compensation>(testComp, headers), Compensation.class);
		assertEquals(200, response.getStatusCodeValue());
		assertEqualComp(testComp, response.getBody());
		
		
		Compensation badTestComp = new Compensation();
		testComp.setEffectiveDate(new Date());
		testComp.setSalary(new BigDecimal(50000));
		testComp.setEmployeeId("16a596ae-edd3-4847-99fe-c4518e82c86f");
		
		//bad GET request
		response = restTemplate.getForEntity(getUrl, Compensation.class, "YEET"); 
		assertEquals(404, response.getStatusCodeValue()); //make sure a 404 is returned on a bad id request
		
		//bad dupp create request 
		response = restTemplate.postForEntity(createUrl, badTestComp, Compensation.class); 
		assertEquals(500, response.getStatusCodeValue()); //make sure a 500 is returned on a dupp request
		
		//bad id create request
		badTestComp.setEmployeeId("YEET");
		response = restTemplate.postForEntity(createUrl, badTestComp, Compensation.class); 
		assertEquals(500, response.getStatusCodeValue()); //make sure a 500 is returned on a bad employee id request

		//bad update request
		response = restTemplate.exchange(updateUrl, HttpMethod.PUT, new HttpEntity<Compensation>(badTestComp, headers), Compensation.class);
		assertEquals(500, response.getStatusCodeValue());
		
	}
	
	private static void assertEqualComp(Compensation expected, Compensation actual) {
		assertEquals(expected.getEffectiveDate(), actual.getEffectiveDate());
		assertEquals(expected.getEmployeeId(), actual.getEmployeeId());
		assertEquals(expected.getSalary(), actual.getSalary());
	}

}
