package com.mindex.challenge.service.impl;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import com.mindex.challenge.controller.response.ReportingStructure;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ReportingServiceImplTest {
	
	private String getUrl;
	
	@LocalServerPort
	private int port;
	
	@Autowired
	private TestRestTemplate restTemplate;
	
	@Before
	public void setup() {
		getUrl = "http://localhost:" + port + "/structure/{id}";
	}
	
	@Test
	public void testAll() {
		//test for a proper value to be returned
		ResponseEntity<ReportingStructure> response = restTemplate.getForEntity(getUrl, ReportingStructure.class, "16a596ae-edd3-4847-99fe-c4518e82c86f");
		assertEquals(4, response.getBody().getNumberOfReports());
		
		//test for a 404 error
		response = restTemplate.getForEntity(getUrl, ReportingStructure.class, "YEET");
		assertEquals(404, response.getStatusCodeValue());
	}

}
